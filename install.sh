#!/bin/sh

#based off of: https://medium.com/better-programming/install-tensorflow-1-13-on-ubuntu-18-04-with-gpu-support-239b36d29070

#cleanup before install
sudo apt-get purge nvidia*
sudo apt-get autoremove
sudo apt-get autoclean
sudo rm -rf /usr/local/cuda*

sudo apt-key adv --fetch-keys
http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" | sudo tee /etc/apt/sources.list.d/cuda.list

sudo apt-get update 
sudo apt-get -o Dpkg::Options::="--force-overwrite" install cuda-10-0 cuda-drivers

#reboot
sudo reboot

echo 'export PATH=/usr/local/cuda-10.0/bin${PATH:+:${PATH}}' >> ~/.bashrc
echo 'export LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}' >> ~/.bashrc
source ~/.bashrc
sudo ldconfig

#test by running 
nvidia-smi
#DRIVERS NOW INSTALLED, THE REST SHOULD BE DOABLE IN DOCKER

#install cuDNN file in this dir
tar -xf cudnn-10.0-linux-x64-v7.5.0.56.tgz
sudo cp -R cuda/include/* /usr/local/cuda-10.0/include
sudo cp -R cuda/lib64/* /usr/local/cuda-10.0/lib64

sudo apt-get install libcupti-dev
echo 'export LD_LIBRARY_PATH=/usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH' >> ~/.bashrc

#install nvtop
sudo apt install cmake libncurses5-dev libncursesw5-dev git
git clone https://github.com/Syllo/nvtop.git
mkdir -p nvtop/build && cd nvtop/build
cmake ..
make
sudo make install # You may need sufficient permission for that (root)

#HERE IS WHERE WE SHOULD CREATE imagifier VIRTUALENV 
#python related
sudo apt-get install python3-numpy python3-dev python3-pip python3-wheel

#tensorflow
pip3 install --user tensorflow-gpu==1.13.1

#print info of install
pip3 show tensorflow-gpu

#test tensorflow in python3
#from tensorflow.python.client import device_lib
#print(device_lib.list_local_devices())

#install keras
pip install numpy scipy
pip install scikit-learn
pip install pillow
pip install h5py
pip install keras

#set up imagifier
cd Virtualenvs
virtualenv tf-imagifier
source tf-imafifier/bin/activate
cd tf-imagifier/lib/python3.6/site-packages/
git clone git@bitbucket.org:imagimob/imagifier.git
cd imagifier
pip install -r requirement.txt

# verify if tensorflow gpu and imagifier is installed in tf-imaigifier
source /home/imagimob/Virtualenvs/tf-imagifier/bin/activate
python
>>import tensorflow as tf
>>tf.test.is_gpu_available()
# if True, then GPU tensorflow is installed 
>>import imagifier 
# if works, then ready to go with imagifier

# update imagifier
cd /home/imagimob/tf-imagifier/lib/python3.6/site-packages/imagifier/
git pull
